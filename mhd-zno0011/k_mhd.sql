
alter procedure vyhledej_spoj (@nazev_odjezdu varchar(30), @nazev_prijezdu varchar(30), @cas_zadan time, @cas_odjezdu time out, @cas_prijezdu time out, @linka_odjezdu integer out)
AS
BEGIN
declare @zastavka_odjezdu integer, @zastavka_prijezdu integer, @linka_prijezdu integer;
	IF exists(select * from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_odjezdu and cas>=@cas_zadan and zlc.cisloLinky in (select zlc.cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_prijezdu and cas>=@cas_zadan))
	begin
		select @cas_odjezdu=cas, @linka_odjezdu=cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_odjezdu and cas>=@cas_zadan and zlc.cisloLinky in (select zlc.cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_prijezdu) order by cas desc;
		select @cas_prijezdu=cas, @linka_prijezdu=cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_prijezdu and zlc.cisloLinky=@linka_odjezdu and cas>=@cas_odjezdu order by cas desc;
	END
END
GO

--declare @cas_odjezdu time, @cas_prijezdu time, @linka_odjezdu integer;
--EXECUTE vyhledej_spoj 'Studentsk�', 'Kr�tk�', '6:00', @cas_odjezdu out, @cas_prijezdu out, @linka_odjezdu out;
--print @cas_odjezdu;
--print @cas_prijezdu;
--print @linka_odjezdu;
--Go

alter procedure vypocti_cenu (@nazev_odjezdu varchar(30), @nazev_prijezdu varchar(30), @cas_zadan time, @cena integer out)
as
declare @cas_odjezdu time, @cas_prijezdu time, @min integer, @linka integer;
begin
	EXECUTE vyhledej_spoj @nazev_odjezdu, @nazev_prijezdu, @cas_zadan, @cas_odjezdu out, @cas_prijezdu out, @linka;
	set @min=(SELECT DATEDIFF(minute, @cas_odjezdu, @cas_prijezdu));
	if @min > 60
		set @cena = 30;

	else if @min > 30
		set @cena = 20;

	else if @min > 15
		set @cena = 17;

	else if @min > 1
		set @cena = 15;

	else
		set @cena = -1;
end
go

alter procedure vlozVozidlo(@typVozidla varchar(30), @znacka char(255), @maxPocet integer, @barierove bit, @rc bigint, @od time, @do time, @cisloL integer)
AS
declare @typLinky varchar(30), @idVozidla integer;
begin
	SELECT	@typLinky=typLinky from Linka where cisloLinky=@cisloL;
	BEGIN TRAN
		INSERT INTO Vozidlo(typVozidla, znacka, maxPocet, barierove, rc) VALUES (@typVozidla, @znacka, @maxPocet, @barierove, @rc);
		SELECT @idVozidla = MAX(idVozidla) from Vozidlo;
		INSERT INTO VozidloLinka VALUES(@od, @do, @idVozidla,@cisloL);
	IF @@ERROR <> 0 or @typLinky <> @typVozidla
		BEGIN
			ROLLBACK;
			print 'rollback';
		END
	ELSE
		BEGIN
			COMMIT;
			print 'commit';
		END
end
GO

alter procedure vlozZastavku(@nazev varchar(30), @barierova bit, @cisloZony integer, @cas time, @cisloLinky integer)
AS
declare @idZastavky integer;
begin
	BEGIN TRAN
		INSERT INTO Zastavka(nazev, barierova, cisloZony) VALUES(@nazev, @barierova, @cisloZony);
		SELECT @idZastavky = MAX(idZastavky) from Zastavka;
		INSERT INTO ZastavkaLinkaCas VALUES(@cas, @cisloLinky, @idZastavky);
	IF @@ERROR <> 0
		BEGIN
			ROLLBACK;
			print 'rollback';
		END
	ELSE
		BEGIN
			COMMIT;
			print 'commit';
		END
end
GO

alter procedure delete_vozidlo(@idVozidla integer)
AS
BEGIN TRAN
	DELETE FROM VozidloLinka WHERE idVozidla=@idVozidla; 
	DELETE FROM Vozidlo WHERE idVozidla=@idVozidla;
	IF @@ERROR <> 0
		ROLLBACK;
	else
		COMMIT;
GO

alter procedure delete_zastavky(@idZastavky integer)
AS
BEGIN TRAN
	DELETE FROM ZastavkaLinkaCas  WHERE idZastavky=@idZastavky; 
	DELETE FROM Zastavka WHERE idZastavky=@idZastavky;
	IF @@ERROR <> 0
		ROLLBACK;
	else
		COMMIT;
GO

alter procedure delete_linky(@cisloLinky integer)
AS
BEGIN TRAN

	DELETE FROM VozidloLinka WHERE cisloLinky=@cisloLinky;
	DELETE FROM ZastavkaLinkaCas WHERE cisloLinky=@cisloLinky; 
	DELETE FROM Linka WHERE cisloLinky=@cisloLinky;
	IF @@ERROR <> 0
		ROLLBACK;
	else
		COMMIT;
GO
