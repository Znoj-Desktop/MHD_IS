﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Linka.aspx.cs" Inherits="mhd.Form.Linka" %>

        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <p><asp:GridView ID="GridViewLinka" runat="server" DataKeyNames="cisloLinky" AutoGenerateColumns="False" AllowPaging="True" DataSourceID="odsLinka">
            <Columns>
                <asp:CommandField ShowSelectButton="True" ShowDeleteButton="True"/>
                <asp:BoundField DataField="CisloLinky" HeaderText="cisloLinky"/>
                <asp:BoundField DataField="TypLinky" HeaderText="typLinky"/>
            </Columns>
        </asp:GridView>

        </p><p>

        <asp:DetailsView ID="DetailsViewLinka" runat="server" AutoGenerateRows="false" DataSourceID="odsLinkaDetail" DataKeyNames="cisloLinky" OnDataBound="DetailsViewVozidlo_OnDataBound">
            <Fields>
                <asp:CommandField ShowInsertButton="True" />
                <asp:BoundField DataField="CisloLinky" HeaderText="cisloLinky"/>
                
                <asp:TemplateField HeaderText="typLinky" SortExpression="typLinky">
                    <ItemTemplate>
                        <asp:Label ID="LabelLinka" runat="server" Text='<%# Eval("TypLinky") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList id="TypLinky" AutoPostBack="True" runat="server" DataTextField="TypLinky" DataValueField="TypLinky" SelectedValue='<%# Bind("TypLinky") %>'>

                          <asp:ListItem Selected="True" Value="Autobus"> Autobus </asp:ListItem>
                          <asp:ListItem Value="Tramvaj"> Tramvaj </asp:ListItem>
                          <asp:ListItem Value="Trolejbus"> Trolejbus </asp:ListItem>
                          <asp:ListItem Value="Vlak"> Vlak </asp:ListItem>

                       </asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateField>
            </Fields>
        </asp:DetailsView></p>
            <p>Zastávky, na kterých zastavuje daná linka</p>

            <asp:BulletedList ID="BulletedList1" runat="server" DataSourceID="odsZastavkaL" DataTextField="Nazev" DataValueField="Nazev" Height="16px" Width="231px">
            </asp:BulletedList>

            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>

        <asp:ObjectDataSource ID="odsLinka" runat="server" TypeName="MHD.Database.LinkaTable" SelectMethod="Select"  DeleteMethod="Delete">
            <DeleteParameters>
                <asp:ControlParameter Type="Int32" Name="cisloLinky" ControlID="GridViewLinka">
                </asp:ControlParameter>
            </DeleteParameters>
        </asp:ObjectDataSource> 

        <asp:ObjectDataSource ID="odsLinkaDetail" runat="server" TypeName="MHD.Database.LinkaTable" DataObjectTypeName="MHD.Database.Linka" SelectMethod="Select" InsertMethod="Insert" UpdateMethod="Update">
            <SelectParameters>
              <asp:ControlParameter PropertyName="SelectedValue" Type="Int32" Name="cisloLinky" ControlID="GridViewLinka" DefaultValue="1">
              </asp:ControlParameter>
            </SelectParameters>
        </asp:ObjectDataSource>

            
              <asp:ObjectDataSource ID="odsZastavkaL" runat="server" TypeName="MHD.Database.ZastavkaTable" SelectMethod="SelectJmenLinky" >
                  <SelectParameters>
                    <asp:ControlParameter PropertyName="SelectedValue" Type="Int32" Name="cisloLinky" ControlID="GridViewLinka" DefaultValue="1" >
                    </asp:ControlParameter>
                  </SelectParameters>
              </asp:ObjectDataSource>
        

        </asp:Content>
