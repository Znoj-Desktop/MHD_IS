﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Smena.aspx.cs" Inherits="mhd.Form.Smena" %>

        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <p><asp:GridView ID="GridViewSmena" runat="server" DataKeyNames="typSmeny" 
            AllowPaging="True" DataSourceID="odsSmena">
            <Columns>
                <asp:CommandField ShowSelectButton="True"/>
            </Columns>
        </asp:GridView></p>

        <p><asp:DetailsView ID="DetailsViewSmena" runat="server"
            AutoGenerateRows="true" DataSourceID="odsSmenaDetail" DataKeyNames="typSmeny">
            <Fields>
                <asp:CommandField ShowInsertButton="True"/>
            </Fields>
        </asp:DetailsView></p>
        <asp:ObjectDataSource ID="odsSmena" runat="server" TypeName="MHD.Database.SmenaTable" 
        SelectMethod="Select">
            <DeleteParameters>
                <asp:ControlParameter Type="String" Name="typSmeny" ControlID="GridViewSmena">
                </asp:ControlParameter>
            </DeleteParameters>
        </asp:ObjectDataSource> 

        <asp:ObjectDataSource ID="odsSmenaDetail" runat="server" TypeName="MHD.Database.SmenaTable" 
        DataObjectTypeName="MHD.Database.Smena" SelectMethod="Select" InsertMethod="Insert">
            <SelectParameters>
              <asp:ControlParameter PropertyName="SelectedValue" Type="String" Name="typSmeny" 
              ControlID="GridViewSmena" DefaultValue="denní">
              </asp:ControlParameter>
            </SelectParameters>
        </asp:ObjectDataSource>

        </asp:Content>
