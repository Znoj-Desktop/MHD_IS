﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ZastavkaLinkaCas.aspx.cs" Inherits="mhd.Form.ZastavkaLinkaCas" %>

        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <p><asp:GridView ID="GridViewZastavkaLinkaCas" runat="server" DataKeyNames="cisloLinky, idZastavky" 
            AllowPaging="True" AutoGenerateColumns="True" DataSourceID="odsZastavkaLinkaCas">
            <Columns>
                <asp:CommandField ShowSelectButton="True" ShowDeleteButton="True"/>
            </Columns>
        </asp:GridView></p>

        <p><asp:DetailsView ID="DetailsViewZastavkaLinkaCas" runat="server"
            AutoGenerateRows="false" DataSourceID="odsZastavkaLinkaCasDetail" DataKeyNames="cisloLinky, idZastavky">
            <Fields>
                <asp:BoundField DataField="Cas" HeaderText="cas"/>
                
                <asp:TemplateField HeaderText="Linka" SortExpression="Linka">
                    <ItemTemplate>
                        <asp:Label ID="LabelLinka" runat="server" Text='<%# Eval("Linka.CisloLinky") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ListLinka" runat="server" DataSourceID="odsLinka"
                            DataTextField="CisloLinky" DataValueField="CisloLinky" SelectedValue='<%# Bind("CisloLinky") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ValidatorLinka" runat="server" ControlToValidate="ListLinka" 
                            ErrorMessage="Linka is required" Display="None">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Zastavka" SortExpression="Zastavka">
                    <ItemTemplate>
                        <asp:Label ID="LabelZastavka" runat="server" Text='<%# Eval("Zastavka.IdZastavky") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ListZastavka" runat="server" DataSourceID="odsZastavka"
                            DataTextField="IdZastavky" DataValueField="IdZastavky" SelectedValue='<%# Bind("IdZastavky") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ValidatorZastavka" runat="server" ControlToValidate="ListZastavka" 
                            ErrorMessage="Zastavka is required" Display="None">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>

                <asp:CommandField ShowEditButton="True" ShowInsertButton="True"/>
            </Fields>
        </asp:DetailsView></p>





            <asp:ObjectDataSource ID="odsZastavkaLinkaCas" runat="server" TypeName="MHD.Database.ZastavkaLinkaCasTable" 
        SelectMethod="Select"  DeleteMethod="Delete">
            <DeleteParameters>
                <asp:ControlParameter Type="Int32" Name="cisloLinky" ControlID="GridViewZastavkaLinkaCas">
                </asp:ControlParameter>
                <asp:ControlParameter Type="Int32" Name="idZastavky" ControlID="GridViewZastavkaLinkaCas">
                </asp:ControlParameter>
            </DeleteParameters>
        </asp:ObjectDataSource> 

        <asp:ObjectDataSource ID="odsZastavkaLinkaCasDetail" runat="server" TypeName="MHD.Database.ZastavkaLinkaCasTable" 
        DataObjectTypeName="MHD.Database.ZastavkaLinkaCas" SelectMethod="Select" InsertMethod="Insert" UpdateMethod="Update">
            <SelectParameters>
              <asp:ControlParameter PropertyName='SelectedDataKey["cisloLinky"]' Type="Int32" Name="cisloLinky" ControlID="GridViewZastavkaLinkaCas">
              </asp:ControlParameter>
                <asp:ControlParameter PropertyName='SelectedDataKey["idZastavky"]' Type="Int32" Name="idZastavky" ControlID="GridViewZastavkaLinkaCas">
              </asp:ControlParameter>
            </SelectParameters>
        </asp:ObjectDataSource>

        <asp:ObjectDataSource ID="odsLinka" runat="server" TypeName="MHD.Database.LinkaTable" SelectMethod="SelectLinek" >
        </asp:ObjectDataSource>

        <asp:ObjectDataSource ID="odsZastavka" runat="server" TypeName="MHD.Database.ZastavkaTable" SelectMethod="Select" >
        </asp:ObjectDataSource>


        </asp:Content>
