﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHD.Database;

namespace mhd.Form
{
    public partial class Cena : System.Web.UI.Page
    {
        Database db;

        protected void Page_Load(object sender, EventArgs e)
        {
            db = new Database();
            db.Connect();
            
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String Od = DropDownList1c.Text;
            String Do = DropDownList2c.Text;
            //spojeni
            vypoctiCenu(Od, Do);
            
        }

        protected void vypoctiCenu(String Od, String Do)
        {
            try
            {
                TimeSpan Cas = TimeSpan.Parse(TextBox1.Text);
                String Cas_parse;

                SqlDataAdapter MyDataAdapter = new SqlDataAdapter(db.CreateCommand("vypocti_cenu"));
                MyDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                MyDataAdapter.SelectCommand.Parameters.AddWithValue("@nazev_odjezdu", Od);
                MyDataAdapter.SelectCommand.Parameters.AddWithValue("@nazev_prijezdu", Do);
                Cas_parse = Cas.Hours + ":" + Cas.Minutes;
                MyDataAdapter.SelectCommand.Parameters.AddWithValue("@cas_zadan", Cas);


                MyDataAdapter.SelectCommand.Parameters.Add("@cena", SqlDbType.VarChar, 5);
                MyDataAdapter.SelectCommand.Parameters["@cena"].Direction = ParameterDirection.Output;
                MyDataAdapter.SelectCommand.ExecuteNonQuery();

                string vysledek1 = (string)MyDataAdapter.SelectCommand.Parameters["@cena"].Value;
                if (Int32.Parse(vysledek1) == -1)
                {
                    Label1.Text = "Cenu nelze vypočítat - spoj nenalezen";
                }
                else
                {
                    Label1.Text = "Cena bude: " + vysledek1 + "Kč";
                }
            }
            catch (Exception exc)
            {
                Label1.Text = "Cenu nelze vypočítat " + exc.Message;
            }
            db.Close();
        }
    }
}