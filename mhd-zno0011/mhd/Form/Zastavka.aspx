﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Zastavka.aspx.cs" Inherits="mhd.Form.Zastavka" %>

        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <p><asp:GridView ID="GridViewZastavka" runat="server" DataKeyNames="idZastavky" 
            AllowPaging="True" DataSourceID="odsZastavka" AutoGenerateColumns="False">
            <Columns>
                <asp:CommandField ShowSelectButton="True" ShowDeleteButton="true"/>
                <asp:BoundField DataField="IdZastavky" HeaderText="IdZastavky" SortExpression="IdZastavky" />
                <asp:BoundField DataField="Nazev" HeaderText="Nazev" SortExpression="Nazev" />
                <asp:BoundField DataField="Barierova" HeaderText="Barierova" SortExpression="Barierova" />
                <asp:BoundField DataField="CisloZony" HeaderText="CisloZony" SortExpression="CisloZony" />
            </Columns>
        </asp:GridView></p>

        <h3>Detail zastavky</h3>
        <p><asp:DetailsView ID="DetailsViewZastavka" runat="server"
            AutoGenerateRows="false" DataSourceID="odsZastavkaDetail" DataKeyNames="idZastavky" OnDataBound="DetailsViewZastavka_OnDataBound">
            <Fields>
                <asp:BoundField DataField="IdZastavky" HeaderText="idZastavky"/>

                <asp:BoundField DataField="Nazev" HeaderText="nazev"/>

                <asp:checkboxfield DataField="Barierova" headertext="barierova"/>  

                <asp:TemplateField HeaderText="Zona, ve ktere bude zastavka lezet" SortExpression="Zona">
                    <ItemTemplate>
                        <asp:Label ID="LabelZona" runat="server" Text='<%# Bind("CisloZony") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ListZona" runat="server" DataSourceID="odsZona" DataTextField="CisloZony" DataValueField="CisloZony" SelectedValue='<%# Bind("CisloZony") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ValidatorZona" runat="server" ControlToValidate="ListZona" ErrorMessage="Zona is required" Display="None">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="Cas" HeaderText="cas"/>

                <asp:TemplateField HeaderText="Linka, ktera bude zastávkou projíždět" SortExpression="Linka">
                    <ItemTemplate>
                        <asp:Label ID="LabelLinka" runat="server" Text='<%# Eval("Linka.CisloLinky") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ListLinka" runat="server" DataSourceID="odsLinka" DataTextField="CisloLinky" DataValueField="CisloLinky" SelectedValue='<%# Bind("CisloLinky") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ValidatorLinka" runat="server" ControlToValidate="ListLinka" ErrorMessage="Linka is required" Display="None">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>

                <asp:CommandField ShowInsertButton="True"/>
            </Fields>
        </asp:DetailsView></p>

        <asp:ObjectDataSource ID="odsZastavka" runat="server" TypeName="MHD.Database.ZastavkaTable" SelectMethod="Select"  DeleteMethod="Delete">
            <DeleteParameters>
                <asp:ControlParameter Type="Int32" Name="idZastavky" ControlID="GridViewZastavka">
                </asp:ControlParameter>
            </DeleteParameters>
        </asp:ObjectDataSource> 

        <asp:ObjectDataSource ID="odsZastavkaDetail" runat="server" TypeName="MHD.Database.ZastavkaTable" 
        DataObjectTypeName="MHD.Database.Zastavka" SelectMethod="Select" InsertMethod="Insert" UpdateMethod="Update">
            <SelectParameters>
              <asp:ControlParameter PropertyName="SelectedValue" Type="Int32" Name="idZastavky" ControlID="GridViewZastavka" DefaultValue="1">
              </asp:ControlParameter>
            </SelectParameters>
        </asp:ObjectDataSource>

        <asp:ObjectDataSource ID="odsZona" runat="server" TypeName="MHD.Database.ZonaTable" SelectMethod="SelectZon" >
        </asp:ObjectDataSource>
            
        <asp:ObjectDataSource ID="odsLinka" runat="server" TypeName="MHD.Database.LinkaTable" SelectMethod="SelectLinek" >
        </asp:ObjectDataSource>
        
        </asp:Content>
