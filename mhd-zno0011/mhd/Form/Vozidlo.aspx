﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Vozidlo.aspx.cs" Inherits="mhd.Form.Vozidlo" %>

        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <h3>Vozidla</h3>
        <p><asp:GridView ID="GridViewVozidlo" runat="server" DataKeyNames="idVozidla" AutoGenerateColumns="False"  
            AllowPaging="True" DataSourceID="odsVozidlo">
            <Columns>
                <asp:CommandField ShowSelectButton="True" ShowDeleteButton="True"/>

                <asp:BoundField DataField="IdVozidla" HeaderText="idVozidla"/>
                <asp:BoundField DataField="TypVozidla" HeaderText="typVozidla"/>
                
                <asp:BoundField DataField="Znacka" HeaderText="znacka"/>

                <asp:BoundField DataField="MaxPocet" HeaderText="maxPocet"/>
                <asp:BoundField DataField="Barierove" HeaderText="barierove"/>
                <asp:TemplateField HeaderText="Zodpovědná osoba" SortExpression="Ridic">
                    <ItemTemplate>
                        <asp:Label ID="LabelRidic" runat="server" Text='<%# Eval("Ridic.Prijmeni") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField> 

            </Columns>
        </asp:GridView></p>

        <h3>Detail vozidla</h3>
        <p><asp:DetailsView ID="DetailsViewVozidlo" runat="server"
            AutoGenerateRows="false" DataSourceID="odsVozidloDetail" DataKeyNames="idVozidla" OnDataBound="DetailsViewVozidlo_OnDataBound">
            <Fields>
                <asp:BoundField DataField="IdVozidla" HeaderText="idVozidla"/>

                <asp:TemplateField HeaderText="Typ vozidla" SortExpression="Vozidlo">
                    <ItemTemplate>
                        <asp:Label ID="LabelVozidlo" runat="server" Text='<%# Eval("TypVozidla") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ListVozidlo" runat="server" DataSourceID="odsVozidloTyp"
                            DataTextField="TypLinky" DataValueField="TypLinky" SelectedValue='<%# Bind("TypVozidla") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ValidatorVozidlo" runat="server" ControlToValidate="ListVozidlo" 
                            ErrorMessage="Typ is required" Display="None">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:BoundField DataField="Znacka" HeaderText="znacka"/>

                <asp:BoundField DataField="MaxPocet" HeaderText="maxPocet"/>

                <asp:checkboxfield DataField="Barierove" headertext="barierove"/>  
            
                <asp:TemplateField HeaderText="Ridic, ktery se bude o vozidlo starat" SortExpression="Ridic">
                    <ItemTemplate>
                        <asp:Label ID="LabelRidic" runat="server" Text='<%# Eval("Ridic.Prijmeni") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ListRidic" runat="server" DataSourceID="odsRidic"
                            DataTextField="Prijmeni" DataValueField="Rc" SelectedValue='<%# Bind("Rc") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ValidatorRidic" runat="server" ControlToValidate="ListRidic" 
                            ErrorMessage="Ridic is required" Display="None">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Linka, na ktere bude vozidlo jezdit" SortExpression="Linka">
                    <ItemTemplate>
                        <asp:Label ID="LabelLinka" runat="server" Text='<%# Eval("Linka.CisloLinky") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ListLinka" runat="server" DataSourceID="odsLinka"
                            DataTextField="CisloLinky" DataValueField="CisloLinky" SelectedValue='<%# Bind("CisloLinky") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ValidatorLinka" runat="server" ControlToValidate="ListLinka" 
                            ErrorMessage="Linka is required" Display="None">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="Od" HeaderText="od" />


                <asp:BoundField DataField="Do" HeaderText="do"/>
            
                <asp:CommandField ShowInsertButton="True"/>
            </Fields>
        </asp:DetailsView></p>

        <asp:ObjectDataSource ID="odsVozidlo" runat="server" TypeName="MHD.Database.VozidloTable" SelectMethod="Select"  DeleteMethod="Delete">
            <DeleteParameters>
                <asp:ControlParameter Type="Int32" Name="idVozidla" ControlID="GridViewVozidlo">
                </asp:ControlParameter>
            </DeleteParameters>
        </asp:ObjectDataSource> 

        <asp:ObjectDataSource ID="odsVozidloDetail" runat="server" TypeName="MHD.Database.VozidloTable" 
        DataObjectTypeName="MHD.Database.Vozidlo" SelectMethod="Select" InsertMethod="Insert" UpdateMethod="Update">
            <SelectParameters>
              <asp:ControlParameter PropertyName="SelectedValue" Type="Int32" Name="idVozidla" ControlID="GridViewVozidlo" DefaultValue="1">
              </asp:ControlParameter>
            </SelectParameters>
        </asp:ObjectDataSource>


        <asp:ObjectDataSource ID="odsRidic" runat="server" TypeName="MHD.Database.RidicTable" SelectMethod="SelectNestarajicichSe" >
        </asp:ObjectDataSource>

        <asp:ObjectDataSource ID="odsLinka" runat="server" TypeName="MHD.Database.LinkaTable" SelectMethod="SelectLinek" >
        </asp:ObjectDataSource>

        <asp:ObjectDataSource ID="odsVozidloTyp" runat="server" TypeName="MHD.Database.LinkaTable" SelectMethod="SelectTypu" >
        </asp:ObjectDataSource>

        </asp:Content>
