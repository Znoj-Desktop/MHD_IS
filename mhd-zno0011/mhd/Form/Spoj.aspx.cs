﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHD.Database;

namespace mhd.Form
{
    public partial class Spoj : System.Web.UI.Page
    {
        Database db;

        protected void Page_Load(object sender, EventArgs e)
        {
            db = new Database();
            db.Connect();
            
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String Od = DropDownList1.Text;
            String Do = DropDownList2.Text;
            //spojeni
            vyhledejSpoj(Od, Do);
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            String Od = DropDownList1.Text;
            String Do = DropDownList2.Text;
            //zpatecni spojeni
            vyhledejSpoj(Do, Od);
        }

        protected void vyhledejSpoj(String Od, String Do)
        {
            try
            {
                TimeSpan Cas = TimeSpan.Parse(TextBox1.Text);
                String Cas_parse;

                SqlDataAdapter MyDataAdapter = new SqlDataAdapter(db.CreateCommand("vyhledej_spoj"));
                MyDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                MyDataAdapter.SelectCommand.Parameters.AddWithValue("@nazev_odjezdu", Od);
                MyDataAdapter.SelectCommand.Parameters.AddWithValue("@nazev_prijezdu", Do);
                Cas_parse = Cas.Hours + ":" + Cas.Minutes;
                MyDataAdapter.SelectCommand.Parameters.AddWithValue("@cas_zadan", Cas);


                MyDataAdapter.SelectCommand.Parameters.Add("@cas_odjezdu", SqlDbType.VarChar, 5);
                MyDataAdapter.SelectCommand.Parameters["@cas_odjezdu"].Direction = ParameterDirection.Output;
                MyDataAdapter.SelectCommand.Parameters.Add("@cas_prijezdu", SqlDbType.VarChar, 5);
                MyDataAdapter.SelectCommand.Parameters["@cas_prijezdu"].Direction = ParameterDirection.Output;
                MyDataAdapter.SelectCommand.Parameters.Add("@linka_odjezdu", SqlDbType.Int);
                MyDataAdapter.SelectCommand.Parameters["@linka_odjezdu"].Direction = ParameterDirection.Output;
                MyDataAdapter.SelectCommand.ExecuteNonQuery();
            
                string vysledek1 = (string)MyDataAdapter.SelectCommand.Parameters["@cas_odjezdu"].Value;
                string vysledek2 = (string)MyDataAdapter.SelectCommand.Parameters["@cas_prijezdu"].Value;
                string vysledek3 = (string)MyDataAdapter.SelectCommand.Parameters["@linka_odjezdu"].Value.ToString();
                Label1.Text = Od + ": " + vysledek1 + "<br />" + Do + ": " + vysledek2 + "<br /> Linka číslo " + vysledek3;
            }
            catch (Exception exc)
            {
                Label1.Text = "Spojení nenalezeno " + exc.Message;
            }
            db.Close();
        }
    }
}