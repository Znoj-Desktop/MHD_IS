﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Zona.aspx.cs" Inherits="mhd.Form.Zona" %>

        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <p><asp:GridView ID="GridViewZona" runat="server" DataKeyNames="cisloZony" 
            AllowPaging="True" DataSourceID="odsZona">
            <Columns>
                <asp:CommandField ShowSelectButton="True"/>
            </Columns>
        </asp:GridView></p>

        <p><asp:DetailsView ID="DetailsViewZona" runat="server"
            AutoGenerateRows="true" DataSourceID="odsZonaDetail" DataKeyNames="cisloZony">
            <Fields>
                <asp:CommandField ShowEditButton="True" ShowInsertButton="True"/>
            </Fields>
        </asp:DetailsView></p>

        <asp:ObjectDataSource ID="odsZona" runat="server" TypeName="MHD.Database.ZonaTable" 
        SelectMethod="Select">
            <DeleteParameters>
                <asp:ControlParameter Type="Int32" Name="cisloZony" ControlID="GridViewZona">
                </asp:ControlParameter>
            </DeleteParameters>
        </asp:ObjectDataSource> 

        <asp:ObjectDataSource ID="odsZonaDetail" runat="server" TypeName="MHD.Database.ZonaTable" 
        DataObjectTypeName="MHD.Database.Zona" SelectMethod="Select" InsertMethod="Insert" UpdateMethod="Update">
            <SelectParameters>
              <asp:ControlParameter PropertyName="SelectedValue" Type="Int32" Name="cisloZony" 
              ControlID="GridViewZona" DefaultValue="1">
              </asp:ControlParameter>
            </SelectParameters>
        </asp:ObjectDataSource>

        </asp:Content>
