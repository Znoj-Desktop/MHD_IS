﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VozidloLinka.aspx.cs" Inherits="mhd.Form.VozidloLinka" %>

        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <p><asp:GridView ID="GridViewVozidloLinka" runat="server" DataKeyNames="idVozidla, cisloLinky" 
            AllowPaging="True" DataSourceID="odsVozidloLinka">
            <Columns>
                <asp:CommandField ShowSelectButton="True" ShowDeleteButton="True"/>
            </Columns>
        </asp:GridView></p>

        <p><asp:DetailsView ID="DetailsViewVozidloLinka" runat="server"
            AutoGenerateRows="false" DataSourceID="odsVozidloLinkaDetail" DataKeyNames="idVozidla, cisloLinky">
            <Fields>
                <asp:CommandField ShowEditButton="True" ShowInsertButton="True"/>
            
                <asp:BoundField DataField="Od" HeaderText="od"/>
                <asp:BoundField DataField="Do" HeaderText="do"/>

                <asp:TemplateField HeaderText="Vozidlo" SortExpression="Vozidlo">
                    <ItemTemplate>
                        <asp:Label ID="LabelVozidlo" runat="server" Text='<%# Eval("IdVozidla") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ListVozidlo" runat="server" DataSourceID="odsVozidlo"
                            DataTextField="IdVozidla" DataValueField="IdVozidla" SelectedValue='<%# Bind("IdVozidla") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ValidatorVozidlo" runat="server" ControlToValidate="ListVozidlo" 
                            ErrorMessage="Vozidlo is required" Display="None">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>
            
                
                <asp:TemplateField HeaderText="Linka" SortExpression="Linka">
                    <ItemTemplate>
                        <asp:Label ID="LabelLinka" runat="server" Text='<%# Eval("CisloLinky") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ListLinka" runat="server" DataSourceID="odsLinka"
                            DataTextField="CisloLinky" DataValueField="CisloLinky" SelectedValue='<%# Bind("CisloLinky") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ValidatorLinka" runat="server" ControlToValidate="ListLinka" 
                            ErrorMessage="Linka is required" Display="None">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>
            
            
            
            </Fields>
        </asp:DetailsView></p>

        <asp:ObjectDataSource ID="odsVozidloLinka" runat="server" TypeName="MHD.Database.VozidloLinkaTable" 
        SelectMethod="Select"  DeleteMethod="Delete">
            <DeleteParameters>
                <asp:ControlParameter Type="Int32" Name="idVozidla" ControlID="GridViewVozidloLinka">
                </asp:ControlParameter>
                <asp:ControlParameter Type="Int32" Name="cisloLinky" ControlID="GridViewVozidloLinka">
                </asp:ControlParameter>
            </DeleteParameters>
        </asp:ObjectDataSource> 

        <asp:ObjectDataSource ID="odsVozidloLinkaDetail" runat="server" TypeName="MHD.Database.VozidloLinkaTable" 
        DataObjectTypeName="MHD.Database.VozidloLinka" SelectMethod="SelectDetail" InsertMethod="Insert" UpdateMethod="Update">
            <SelectParameters>
              <asp:ControlParameter PropertyName='SelectedDataKey["idVozidla"]' Type="Int32" Name="idVozidla"
              ControlID="GridViewVozidloLinka" DefaultValue="0">
              </asp:ControlParameter>
                <asp:ControlParameter PropertyName='SelectedDataKey["cisloLinky"]'  Type="Int32" Name="cisloLinky"
              ControlID="GridViewVozidloLinka" DefaultValue="0">
              </asp:ControlParameter>
            </SelectParameters>
        </asp:ObjectDataSource>



            <asp:ObjectDataSource ID="odsLinka" runat="server" TypeName="MHD.Database.LinkaTable" SelectMethod="SelectLinek" >
        </asp:ObjectDataSource>

            <asp:ObjectDataSource ID="odsVozidlo" runat="server" TypeName="MHD.Database.VozidloTable" SelectMethod="SelectVozidel" >
        </asp:ObjectDataSource>


        </asp:Content>
