﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Ridic.aspx.cs" Inherits="mhd.Form.Ridic" %>

        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <p><asp:GridView ID="GridViewRidic" runat="server" DataKeyNames="rc" 
            AllowPaging="True" DataSourceID="odsRidic">
            <Columns>
                <asp:CommandField ShowSelectButton="True" ShowDeleteButton="True"/>
            </Columns>
        </asp:GridView></p>

        <p><asp:DetailsView ID="DetailsViewRidic" runat="server"
            AutoGenerateRows="true" DataSourceID="odsRidicDetail" DataKeyNames="rc">
            <Fields>
                <asp:CommandField ShowEditButton="True" ShowInsertButton="True"/>
            </Fields>
        </asp:DetailsView></p>

        <asp:ObjectDataSource ID="odsRidic" runat="server" TypeName="MHD.Database.RidicTable" 
        SelectMethod="Select"  DeleteMethod="Delete">
            <DeleteParameters>
                <asp:ControlParameter Type="Int64" Name="rc" ControlID="GridViewRidic">
                </asp:ControlParameter>
            </DeleteParameters>
        </asp:ObjectDataSource> 

        <asp:ObjectDataSource ID="odsRidicDetail" runat="server" TypeName="MHD.Database.RidicTable" 
        DataObjectTypeName="MHD.Database.Ridic" SelectMethod="Select" InsertMethod="Insert" UpdateMethod="Update">
            <SelectParameters>
              <asp:ControlParameter PropertyName="SelectedValue" Type="Int64" Name="rc" 
              ControlID="GridViewRidic" DefaultValue="1">
              </asp:ControlParameter>
            </SelectParameters>
        </asp:ObjectDataSource>

        </asp:Content>
