﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Tento kód byl vygenerován nástrojem.
//
//     Změny tohoto souboru mohou způsobit nekorektní chování a budou ztraceny, jestliže
//     dojde k novému generování kódu.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mhd.Account
{


    public partial class Register
    {

        /// <summary>
        /// Ovládací prvek RegisterUser.
        /// </summary>
        /// <remarks>
        /// Automaticky vytvořené pole.
        /// Pro úpravu přesuňte deklaraci pole ze souboru návrháře do souboru pomocného kódu.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CreateUserWizard RegisterUser;

        /// <summary>
        /// Ovládací prvek RegisterUserWizardStep.
        /// </summary>
        /// <remarks>
        /// Automaticky vytvořené pole.
        /// Pro úpravu přesuňte deklaraci pole ze souboru návrháře do souboru pomocného kódu.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CreateUserWizardStep RegisterUserWizardStep;
    }
}
