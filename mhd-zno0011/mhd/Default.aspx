﻿<%@ Page Title="Domovská stránka" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="mhd._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Vítá vás technologie ASP.NET!
    </h2>
    <p>
        Více informací o technologii ASP.NET naleznete na adrese <a href="http://www.asp.net" title="ASP.NET Website">www.asp.net</a>.
    </p>
    <p>
        Dokumentaci k technologii ASP.NET naleznete na stránkách <a href="http://go.microsoft.com/fwlink/?LinkID=152368&amp;clcid=0x405"
            title="MSDN ASP.NET Docs">MSDN</a>.
    </p>
</asp:Content>
