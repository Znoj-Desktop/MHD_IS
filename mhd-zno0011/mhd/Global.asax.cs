﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace mhd
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Kód spouštěný při spuštění aplikace

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Kód spouštěný při ukončení aplikace

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Kód spouštěný při výskytu neošetřené výjimky

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Kód spouštěný při spuštění nové relace

        }

        void Session_End(object sender, EventArgs e)
        {
            // Kód spouštěný při ukončení relace. 
            // Poznámka: Tato událost je vyvolána, pouze pokud je mód oddílu sessionstate
            // nastaven na InProc v souboru Web.config. Jestliže je mód relace nastaven na StateServer 
            // nebo SQLServer, událost nebude vyvolána.

        }

    }
}
