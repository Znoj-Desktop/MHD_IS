﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MHD.Database
{
    public class ZonaTable
    {
        public static String TABLE_NAME = "Zona";
        public String SQL_SELECT = "SELECT * FROM Zona";
        public String SQL_SELECT_ZON = "SELECT cisloZony FROM Zona";
        public String SQL_SELECT_ID = "SELECT * FROM Zona WHERE cisloZony=@cisloZony";
        public String SQL_INSERT = "INSERT INTO Zona VALUES (@cisloZony, @mesto)";
        public String SQL_UPDATE = "UPDATE Zona SET cisloZony=@cisloZony, mesto=@mesto WHERE cisloZony=@cisloZony";

        public ZonaTable()
        {
        }

        public int Insert(Zona zona)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_INSERT); 
            PrepareCommand(command, zona);
            int ret = db.ExecuteNonQuery(command);
            db.Close();
            return ret;
        }

        public int Update(Zona zona)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_UPDATE);
            PrepareCommand(command, zona);
            int ret = db.ExecuteNonQuery(command);
            db.Close();
            return ret;
        }

        private void PrepareCommand(SqlCommand command, Zona zona)
        {
            command.Parameters.Add(new SqlParameter("@cisloZony", SqlDbType.Int));
            command.Parameters["@cisloZony"].Value = zona.CisloZony;

            command.Parameters.Add(new SqlParameter("@mesto", SqlDbType.Bit));
            command.Parameters["@mesto"].Value = zona.Mesto;
        }


        public Collection<Zona> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            SqlDataReader reader = db.Select(command);

            Collection<Zona> zony = Read(reader);
            reader.Close();
            db.Close();
            return zony;
        }

        public Collection<Zona> SelectZon()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT_ZON);
            SqlDataReader reader = db.Select(command);

            Collection<Zona> zony = ReadZon(reader);
            reader.Close();
            db.Close();
            return zony;
        }

        private Collection<Zona> ReadZon(SqlDataReader reader)
        {
            Collection<Zona> zony = new Collection<Zona>();

            while (reader.Read())
            {
                Zona zona = new Zona();
                zona.CisloZony = reader.GetInt32(0);

                zony.Add(zona);
            }
            return zony;
        }

        public Zona Select(int cisloZony)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_SELECT_ID);

            command.Parameters.Add(new SqlParameter("@cisloZony", SqlDbType.Int));
            command.Parameters["@cisloZony"].Value = cisloZony;
            SqlDataReader reader = db.Select(command);

            Collection<Zona> zony = Read(reader);
            Zona zona = null;
            if (zony.Count == 1)
            {
                zona = zony[0];
            }
            reader.Close();
            db.Close();
            return zona;
        }




        private Collection<Zona> Read(SqlDataReader reader)
        {
            Collection<Zona> zony = new Collection<Zona>();

            while (reader.Read())
            {
                Zona zona = new Zona();
                zona.CisloZony = reader.GetInt32(0);
                zona.Mesto = reader.GetBoolean(1);

                zony.Add(zona);
            }
            return zony;
        }
    }

}