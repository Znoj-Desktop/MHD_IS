﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MHD.Database
{
    public class LinkaTable
    {
        public static String TABLE_NAME = "Linka";
        public String SQL_SELECT = "SELECT * FROM Linka";
        public String SQL_SELECT_LINEK = "SELECT cisloLinky FROM Linka";
        public String SQL_SELECT_TYPU = "SELECT typLinky FROM Linka GROUP BY typLinky";
        public String SQL_SELECT_ID = "SELECT * FROM Linka WHERE cisloLinky=@cisloLinky";
        public String SQL_INSERT = "INSERT INTO Linka(typLinky) VALUES (@typLinky)";
        public String SQL_DELETE_ID = "DELETE FROM Linka WHERE cisloLinky=@cisloLinky";
        public String SQL_UPDATE = "UPDATE Linka SET cisloLinky=@cisloLinky, typLinky=@typLinky WHERE cisloLinky=@cisloLinky";

        public LinkaTable()
        {

        }

        public int Insert(Linka linka)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_INSERT);
            int ret = 0;
            try
            {
                PrepareCommand(command, linka, true);
                ret = db.ExecuteNonQuery(command);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                db.Close();
            }
            return ret;
        }

        public int Update(Linka linka)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_UPDATE);
            int ret = 0;
            try
            {
                PrepareCommand(command, linka, false);
                ret = db.ExecuteNonQuery(command);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                db.Close();
            }
            return ret;
        }

        private void PrepareCommand(SqlCommand command, Linka linka, bool vkladani)
        {
            if (!vkladani)
            {
                command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
                command.Parameters["@cisloLinky"].Value = linka.CisloLinky;
            }
            command.Parameters.Add(new SqlParameter("@typLinky", SqlDbType.VarChar, linka.TypLinky.Length));
            command.Parameters["@typLinky"].Value = linka.TypLinky;
        }


        public Collection<Linka> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            SqlDataReader reader = db.Select(command);

            Collection<Linka> linky = Read(reader);
            reader.Close();
            db.Close();
            return linky;
        }

        public Collection<Linka> SelectLinek()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT_LINEK);
            SqlDataReader reader = db.Select(command);

            Collection<Linka> linky = ReadLinek(reader);
            reader.Close();
            db.Close();
            return linky;
        }

        public Collection<Linka> SelectTypu()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT_TYPU);
            SqlDataReader reader = db.Select(command);

            Collection<Linka> linky = ReadTypu(reader);
            reader.Close();
            db.Close();
            return linky;
        }

        private Collection<Linka> ReadLinek(SqlDataReader reader)
        {
            Collection<Linka> linky = new Collection<Linka>();

            while (reader.Read())
            {
                Linka linka = new Linka();
                linka.CisloLinky = reader.GetInt32(0);

                linky.Add(linka);
            }
            return linky;
        }

        private Collection<Linka> ReadTypu(SqlDataReader reader)
        {
            Collection<Linka> linky = new Collection<Linka>();

            while (reader.Read())
            {
                Linka linka = new Linka();
                linka.TypLinky = reader.GetString(0);

                linky.Add(linka);
            }
            return linky;
        }


        public Linka Select(int cisloLinky)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_SELECT_ID);

            command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
            command.Parameters["@cisloLinky"].Value = cisloLinky;
            SqlDataReader reader = db.Select(command);

            Collection<Linka> linky = Read(reader);
            Linka linka = null;
            if (linky.Count == 1)
            {
                linka = linky[0];
            }
            reader.Close();
            db.Close();
            return linka;
        }




        private Collection<Linka> Read(SqlDataReader reader)
        {
            Collection<Linka> linky = new Collection<Linka>();

            while (reader.Read())
            {
                Linka linka = new Linka();
                linka.CisloLinky = reader.GetInt32(0);
                linka.TypLinky = reader.GetString(1);

                linky.Add(linka);
            }
            return linky;
        }

        public int Delete(int cisloLinky)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand("EXECUTE delete_linky @cisloLinky;");
            int ret = 0;
            try
            {
                command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
                command.Parameters["@cisloLinky"].Value = cisloLinky;
                ret = db.ExecuteNonQuery(command);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                db.Close();
            }
            return ret;
        }
    }

}