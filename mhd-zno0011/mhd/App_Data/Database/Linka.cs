﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MHD.Database
{
    public class Linka
    {
        public int CisloLinky{ get; set; }
        public string TypLinky{ get; set; }
        public Zastavka Zastavka { get; set; }
        public int IdZastavky { get; set; }

        public static int LEN_ATTR_typLinky = 30;
    }

}