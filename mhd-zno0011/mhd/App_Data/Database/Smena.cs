﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MHD.Database
{
    public class Smena
    {
        public string TypSmeny{ get; set; }
        public TimeSpan Zacatek { get; set; }
        public TimeSpan Konec { get; set; }
        public int Hodin { get; set; }

    }

}