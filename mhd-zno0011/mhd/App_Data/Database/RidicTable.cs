﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MHD.Database
{
    public class RidicTable
    {
        public static String TABLE_NAME = "Ridic";
        public String SQL_SELECT = "SELECT * FROM Ridic";
        public String SQL_SELECT_ID = "SELECT * FROM Ridic WHERE rc=@rc";
        public String SQL_SELECT_NS = "select * from ridic r where r.rc not in (Select rc from vozidlo)";
        public String SQL_INSERT = "INSERT INTO Ridic VALUES (@rc, @jmeno, @prijmeni, @bydliste, @tel, @pohlavi, @heslo, @plat)";
        public String SQL_DELETE_ID = "DELETE FROM Ridic WHERE rc=@rc";
        public String SQL_UPDATE = "UPDATE Ridic SET rc=@rc, jmeno=@jmeno, prijmeni=@prijmeni, bydliste=@bydliste, tel=@tel, pohlavi=@pohlavi, heslo=@heslo, plat=@plat WHERE rc=@rc";

        public RidicTable()
        {
        }

        public int Insert(Ridic ridic)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_INSERT);
            PrepareCommand(command, ridic);
            int ret = db.ExecuteNonQuery(command);
            db.Close();
            return ret;
        }

        public int Update(Ridic ridic)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_UPDATE);
            PrepareCommand(command, ridic);
            int ret = db.ExecuteNonQuery(command);
            db.Close();
            return ret;
        }

        private void PrepareCommand(SqlCommand command, Ridic ridic)
        {
            command.Parameters.Add(new SqlParameter("@rc", SqlDbType.BigInt));
            command.Parameters["@rc"].Value = ridic.Rc;

            command.Parameters.Add(new SqlParameter("@jmeno", SqlDbType.VarChar, ridic.Jmeno.Length));
            command.Parameters["@jmeno"].Value = ridic.Jmeno;

            command.Parameters.Add(new SqlParameter("@prijmeni", SqlDbType.VarChar, ridic.Prijmeni.Length));
            command.Parameters["@prijmeni"].Value = ridic.Prijmeni;

            command.Parameters.Add(new SqlParameter("@bydliste", SqlDbType.VarChar, ridic.Bydliste.Length));
            command.Parameters["@bydliste"].Value = ridic.Bydliste;

            command.Parameters.Add(new SqlParameter("@tel", SqlDbType.BigInt));
            command.Parameters["@tel"].Value = ridic.Tel;

            command.Parameters.Add(new SqlParameter("@pohlavi", SqlDbType.VarChar, ridic.Pohlavi.Length));
            command.Parameters["@pohlavi"].Value = ridic.Pohlavi;


            command.Parameters.Add(new SqlParameter("@heslo", SqlDbType.VarChar, ridic.Heslo.Length));
            command.Parameters["@heslo"].Value = ridic.Heslo;

            
            command.Parameters.Add(new SqlParameter("@plat", SqlDbType.Int));
            command.Parameters["@plat"].Value = ridic.Plat;
        }


        public Collection<Ridic> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            SqlDataReader reader = db.Select(command);

            Collection<Ridic> ridici = Read(reader);
            reader.Close();
            db.Close();
            return ridici;
        }

        public Collection<Ridic> SelectNestarajicichSe()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT_NS);
            SqlDataReader reader = db.Select(command);

            Collection<Ridic> ridici = Read(reader);
            reader.Close();
            db.Close();
            return ridici;
        }

        public Ridic Select(long rc)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_SELECT_ID);

            command.Parameters.Add(new SqlParameter("@rc", SqlDbType.BigInt));
            command.Parameters["@rc"].Value = rc;
            SqlDataReader reader = db.Select(command);

            Collection<Ridic> ridici = Read(reader);
            Ridic ridic = null;
            if (ridici.Count == 1)
            {
                ridic = ridici[0];
            }
            reader.Close();
            db.Close();
            return ridic;
        }




        private Collection<Ridic> Read(SqlDataReader reader)
        {
            Collection<Ridic> ridici = new Collection<Ridic>();

            while (reader.Read())
            {
                Ridic ridic = new Ridic();
                ridic.Rc = reader.GetInt64(0);
                ridic.Jmeno = reader.GetString(1);
                ridic.Prijmeni = reader.GetString(2);
                ridic.Bydliste = reader.GetString(3);
                ridic.Tel = reader.GetInt64(4);
                ridic.Pohlavi = reader.GetString(5);
                ridic.Heslo = reader.GetString(6);
                ridic.Plat = reader.GetInt32(7);

                ridici.Add(ridic);
            }
            return ridici;
        }

        public int Delete(long rc)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_DELETE_ID);

            command.Parameters.Add(new SqlParameter("@rc", SqlDbType.BigInt));
            command.Parameters["@rc"].Value = rc;
            int ret = db.ExecuteNonQuery(command);

            db.Close();
            return ret;
        }
    }

}