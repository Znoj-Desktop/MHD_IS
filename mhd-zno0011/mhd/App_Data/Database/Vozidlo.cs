﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MHD.Database
{
    public class Vozidlo
    {
        public int IdVozidla{ get; set; }
        public string TypVozidla{ get; set; }
        public string Znacka { get; set; }
        public int MaxPocet { get; set; }
        public bool Barierove { get; set; }
        public long Rc { get; set; }
        public Ridic Ridic { get; set; }
        public int CisloLinky { get; set; }
        public TimeSpan Od { get; set; }
        public TimeSpan Do { get; set; }
        public Linka Linka { get; set; }

    }

}