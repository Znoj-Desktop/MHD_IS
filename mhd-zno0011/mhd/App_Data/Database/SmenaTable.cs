﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MHD.Database
{
    public class SmenaTable
    {
        public static String TABLE_NAME = "Smena";
        public String SQL_SELECT = "SELECT * FROM Smena";
        public String SQL_SELECT_ID = "SELECT * FROM Smena WHERE typSmeny=@typSmeny";
        public String SQL_INSERT = "INSERT INTO Smena VALUES (@typSmeny, @zacatek, @konec, @hodin)";

        public SmenaTable()
        {
        }

        public int Insert(Smena smena)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_INSERT); 
            PrepareCommand(command, smena);
            int ret = db.ExecuteNonQuery(command);
            db.Close();
            return ret;
        }

        private void PrepareCommand(SqlCommand command, Smena smena)
        {
            command.Parameters.Add(new SqlParameter("@typSmeny", SqlDbType.VarChar, smena.TypSmeny.Length));
            command.Parameters["@typSmeny"].Value = smena.TypSmeny;

            command.Parameters.Add(new SqlParameter("@zacatek", SqlDbType.VarChar, 6));
            command.Parameters["@zacatek"].Value = smena.Zacatek.Hours + ":" + smena.Zacatek.Minutes;

            command.Parameters.Add(new SqlParameter("@konec", SqlDbType.VarChar, 6));
            command.Parameters["@konec"].Value = smena.Konec.Hours + ":" + smena.Konec.Minutes;

            command.Parameters.Add(new SqlParameter("@hodin", SqlDbType.Int));
            if (smena.Hodin == 0)
            {
                command.Parameters["@hodin"].Value = Math.Abs(smena.Konec.Hours - smena.Zacatek.Hours);
            }
            else
            {
                command.Parameters["@hodin"].Value = smena.Hodin;
            }
        }


        public Collection<Smena> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            SqlDataReader reader = db.Select(command);

            Collection<Smena> smeny = Read(reader);
            reader.Close();
            db.Close();
            return smeny;
        }

        public Smena Select(string typSmeny)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_SELECT_ID);

            command.Parameters.Add(new SqlParameter("@typSmeny", SqlDbType.VarChar, typSmeny.Length));
            command.Parameters["@typSmeny"].Value = typSmeny;
            SqlDataReader reader = db.Select(command);

            Collection<Smena> smeny = Read(reader);
            Smena smena = null;
            if (smeny.Count == 1)
            {
                smena = smeny[0];
            }
            reader.Close();
            db.Close();
            return smena;
        }




        private Collection<Smena> Read(SqlDataReader reader)
        {
            Collection<Smena> smeny = new Collection<Smena>();

            while (reader.Read())
            {
                Smena smena = new Smena();
                smena.TypSmeny = reader.GetString(0);
                smena.Zacatek = reader.GetTimeSpan(1);
                smena.Konec = reader.GetTimeSpan(2);
                smena.Hodin = reader.GetInt32(3);

                smeny.Add(smena);
            }
            return smeny;
        }
    }

}