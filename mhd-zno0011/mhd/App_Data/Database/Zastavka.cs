﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MHD.Database
{
    public class Zastavka
    {
        public int IdZastavky { get; set; }
        public string Nazev { get; set; }
        public bool Barierova{ get; set; }
        public int CisloZony { get; set; }
        public Zona Zona { get; set; }
        public int CisloLinky { get; set; }
        public Linka Linka { get; set; }
        public TimeSpan Cas { get; set; }
        public string Odkud { get; set; }
        public string Kam { get; set; }

        public static int LEN_ATTR_Nazev = 30;

    }

}