﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MHD.Database
{
    public class VozidloTable
    {
        public static String TABLE_NAME = "Vozidlo";
        public String SQL_SELECT = "SELECT v.idVozidla, v.typVozidla, v.znacka, v.maxPocet, v.barierove, v.rc, r.prijmeni FROM Vozidlo v join Ridic r on r.rc=v.rc";
        public String SQL_SELECT_ID = "SELECT v.idVozidla, v.typVozidla, v.znacka, v.maxPocet, v.barierove, v.rc, r.prijmeni FROM Vozidlo v join Ridic r on r.rc=v.rc WHERE idVozidla=@idVozidla";
        public String SQL_SELECT_VOZIDEL = "SELECT idVozidla FROM Vozidlo";
        public String SQL_INSERT = "INSERT INTO Vozidlo(typVozidla, znacka, maxPocet, barierove, rc)  VALUES (@typVozidla, @znacka, @maxPocet, @barierove, @rc)";
        public String SQL_UPDATE = "UPDATE Vozidlo SET idVozidla=@idVozidla, typVozidla=@typVozidla, znacka=@znacka, maxPocet=@maxPocet, barierove=@barierove, rc=@rc WHERE idVozidla=@idVozidla";

        public VozidloTable()
        {
        }

        public int Insert(Vozidlo vozidlo)
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand("EXECUTE vlozVozidlo @typVozidla, @znacka, @maxPocet, @barierove, @rc, @od, @do, @cisloLinky;");

            int ret = 0;
            try
            {
                PrepareCommand(command, vozidlo, true);
                ret = db.ExecuteNonQuery(command);
                Console.WriteLine("SQL OK");
            }
            catch (Exception e)
            {
                Console.WriteLine("SQL Error" + e.Message.ToString());
            }
            finally
            {
                db.Close();
            }
            return ret;
        }


        public int Update(Vozidlo vozidlo)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_UPDATE);
            int ret = 0;
            try
            {
                PrepareCommand(command, vozidlo, false);
                ret = db.ExecuteNonQuery(command);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                db.Close();
            }
            return ret;
        }

        private void PrepareCommand(SqlCommand command, Vozidlo vozidlo, bool vkladani)
        {
            if (!vkladani)
            {

                command.Parameters.Add(new SqlParameter("@idVozidla", SqlDbType.Int));
                command.Parameters["@idVozidla"].Value = vozidlo.IdVozidla;
            }
            
            command.Parameters.Add(new SqlParameter("@typVozidla", SqlDbType.VarChar, vozidlo.TypVozidla.Length));
            command.Parameters["@typVozidla"].Value = vozidlo.TypVozidla;
            
            command.Parameters.Add(new SqlParameter("@znacka", SqlDbType.VarChar, vozidlo.Znacka.Length));
            command.Parameters["@znacka"].Value = vozidlo.Znacka;

            command.Parameters.Add(new SqlParameter("@maxPocet", SqlDbType.Int));
            command.Parameters["@maxPocet"].Value = vozidlo.MaxPocet;

            command.Parameters.Add(new SqlParameter("@barierove", SqlDbType.Bit));
            command.Parameters["@barierove"].Value = vozidlo.Barierove;

            command.Parameters.Add(new SqlParameter("@rc", SqlDbType.BigInt));
            command.Parameters["@rc"].Value = vozidlo.Rc;

            command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
            command.Parameters["@cisloLinky"].Value = vozidlo.CisloLinky;
            
            command.Parameters.Add(new SqlParameter("@od", SqlDbType.VarChar, 6));
            command.Parameters["@od"].Value = vozidlo.Od.Hours + ":" + vozidlo.Od.Minutes;

            command.Parameters.Add(new SqlParameter("@do", SqlDbType.VarChar, 6));
            command.Parameters["@do"].Value = vozidlo.Do.Hours + ":" + vozidlo.Do.Minutes;
                        
        }


        public Collection<Vozidlo> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);


            SqlDataReader reader = db.Select(command);

            Collection<Vozidlo> vozidla = Read(reader);
            reader.Close();
            db.Close();
            return vozidla;
        }

        public Vozidlo Select(int idVozidla)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_SELECT_ID);

            command.Parameters.Add(new SqlParameter("@idVozidla", SqlDbType.Int));
            command.Parameters["@idVozidla"].Value = idVozidla;
            SqlDataReader reader = db.Select(command);

            Collection<Vozidlo> vozidla = Read(reader);
            Vozidlo vozidlo = null;
            if (vozidla.Count == 1)
            {
                vozidlo = vozidla[0];
            }
            reader.Close();
            db.Close();
            return vozidlo;
        }

        public Collection<Vozidlo> SelectVozidel()
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_SELECT_VOZIDEL);

            SqlDataReader reader = db.Select(command);
            Collection<Vozidlo> vozidla = new Collection<Vozidlo>();

            while (reader.Read())
            {
                Vozidlo vozidlo = new Vozidlo();
                vozidlo.IdVozidla = reader.GetInt32(0);
                vozidla.Add(vozidlo);
            }
           
            reader.Close();
            db.Close();
            return vozidla;
        }



        private Collection<Vozidlo> Read(SqlDataReader reader)
        {
            Collection<Vozidlo> vozidla = new Collection<Vozidlo>();

            while (reader.Read())
            {
                Vozidlo vozidlo = new Vozidlo();
                vozidlo.IdVozidla = reader.GetInt32(0);
                vozidlo.TypVozidla = reader.GetString(1);
                vozidlo.Znacka = reader.GetString(2);
                vozidlo.MaxPocet = reader.GetInt32(3);
                vozidlo.Barierove = reader.GetBoolean(4);
                vozidlo.Rc = reader.GetInt64(5);
                vozidlo.Ridic = new Ridic();
                vozidlo.Ridic.Rc = vozidlo.Rc;
                vozidlo.Ridic.Prijmeni = reader.GetString(6);
                vozidlo.CisloLinky = 0;
                vozidlo.Od = TimeSpan.Zero;
                vozidlo.Do = TimeSpan.Zero;
                vozidla.Add(vozidlo);
            }
            return vozidla;
        }

        public int Delete(int idVozidla)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand("EXECUTE delete_vozidlo @idVozidla;");

            int ret = 0;
            try
            {
                command.Parameters.Add(new SqlParameter("@idVozidla", SqlDbType.Int));
                command.Parameters["@idVozidla"].Value = idVozidla;

                ret = db.ExecuteNonQuery(command);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                db.Close();
            }
            return ret;
        }
    }

}