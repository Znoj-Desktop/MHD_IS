﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MHD.Database
{
    public class ZastavkaLinkaCas
    {
        public TimeSpan Cas { get; set; }
        public int CisloLinky { get; set; }
        public Linka Linka { get; set; }
        public int IdZastavky { get; set; }
        public Zastavka Zastavka { get; set; }
        
    }

}