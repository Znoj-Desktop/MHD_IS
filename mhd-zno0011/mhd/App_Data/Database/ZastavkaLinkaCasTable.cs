﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MHD.Database
{
    public class ZastavkaLinkaCasTable
    {
        public static String TABLE_NAME = "ZastavkaLinkaCas";
        public String SQL_SELECT = "SELECT * FROM ZastavkaLinkaCas";
        public String SQL_SELECT_ID = "SELECT * FROM ZastavkaLinkaCas WHERE cisloLinky=@cisloLinky and idZastavky=@idZastavky";
        public String SQL_INSERT = "INSERT INTO ZastavkaLinkaCas VALUES (@cas, @cisloLinky, @idZastavky)";
        public String SQL_DELETE_ID = "DELETE FROM ZastavkaLinkaCas WHERE cisloLinky=@cisloLinky and idZastavky=@idZastavky";
        public String SQL_UPDATE = "UPDATE ZastavkaLinkaCas SET cas=@cas, cisloLinky=@cisloLinky, idZastavky=@idZastavky WHERE cisloLinky=@cisloLinky and idZastavky=@idZastavky";

        public ZastavkaLinkaCasTable()
        {
        }

        public int Insert(ZastavkaLinkaCas zastavkaLinkaCas)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_INSERT); 
            PrepareCommand(command, zastavkaLinkaCas);
            int ret = db.ExecuteNonQuery(command);
            db.Close();
            return ret;
        }

        public int Update(ZastavkaLinkaCas zastavkaLinkaCas)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_UPDATE);
            PrepareCommand(command, zastavkaLinkaCas);
            int ret = db.ExecuteNonQuery(command);
            db.Close();
            return ret;
        }

        private void PrepareCommand(SqlCommand command, ZastavkaLinkaCas zastavkaLinkaCas)
        {
            command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
            command.Parameters["@cisloLinky"].Value = zastavkaLinkaCas.CisloLinky;

            command.Parameters.Add(new SqlParameter("@idZastavky", SqlDbType.Int));
            command.Parameters["@idZastavky"].Value = zastavkaLinkaCas.IdZastavky;

            command.Parameters.Add(new SqlParameter("@cas", SqlDbType.VarChar, 6));
            command.Parameters["@cas"].Value = zastavkaLinkaCas.Cas.Hours + ":" + zastavkaLinkaCas.Cas.Minutes;

        }


        public Collection<ZastavkaLinkaCas> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            SqlDataReader reader = db.Select(command);

            Collection<ZastavkaLinkaCas> zastavkaLinkaCasy = Read(reader);
            reader.Close();
            db.Close();
            return zastavkaLinkaCasy;
        }

        public ZastavkaLinkaCas Select(int cisloLinky, int idZastavky)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_SELECT_ID);

            command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
            command.Parameters["@cisloLinky"].Value = cisloLinky;

            command.Parameters.Add(new SqlParameter("@idZastavky", SqlDbType.Int));
            command.Parameters["@idZastavky"].Value = idZastavky;
            SqlDataReader reader = db.Select(command);

            Collection<ZastavkaLinkaCas> zastavkaLinkaCasy = Read(reader);
            ZastavkaLinkaCas zastavkaLinkaCas = null;
            if (zastavkaLinkaCasy.Count == 1)
            {
                zastavkaLinkaCas = zastavkaLinkaCasy[0];
            }
            reader.Close();
            db.Close();
            return zastavkaLinkaCas;
        }




        private Collection<ZastavkaLinkaCas> Read(SqlDataReader reader)
        {
            Collection<ZastavkaLinkaCas> zastavkaLinkaCasy = new Collection<ZastavkaLinkaCas>();

            while (reader.Read())
            {
                ZastavkaLinkaCas zastavkaLinkaCas = new ZastavkaLinkaCas();
                zastavkaLinkaCas.Cas = reader.GetTimeSpan(0);
                zastavkaLinkaCas.CisloLinky = reader.GetInt32(1);
                zastavkaLinkaCas.Linka = new Linka();
                zastavkaLinkaCas.Linka.CisloLinky = zastavkaLinkaCas.CisloLinky;
                zastavkaLinkaCas.IdZastavky = reader.GetInt32(2);
                zastavkaLinkaCas.Zastavka = new Zastavka();
                zastavkaLinkaCas.Zastavka.IdZastavky = zastavkaLinkaCas.IdZastavky;

                zastavkaLinkaCasy.Add(zastavkaLinkaCas);
            }
            return zastavkaLinkaCasy;
        }

        public int Delete(int cisloLinky, int idZastavky)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_DELETE_ID);

            command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
            command.Parameters["@cisloLinky"].Value = cisloLinky;

            command.Parameters.Add(new SqlParameter("@idZastavky", SqlDbType.Int));
            command.Parameters["@idZastavky"].Value = cisloLinky;
            int ret = db.ExecuteNonQuery(command);

            db.Close();
            return ret;
        }
    }

}