﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MHD.Database
{
    public class ZastavkaTable
    {
        public static String TABLE_NAME = "Zastavka";
        public String SQL_SELECT = "SELECT * FROM Zastavka";
        public String SQL_SELECT_JMENA = "SELECT nazev FROM Zastavka";
        public String SQL_SELECT_JMEN_LINKY = "SELECT z.nazev FROM Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky = z.idZastavky where zlc.cisloLinky=@cisloLinky GROUP BY z.nazev";
        public String SQL_SELECT_ID = "SELECT * FROM Zastavka WHERE idZastavky=@idZastavky";
        public String SQL_INSERT = "INSERT INTO Zastavka(nazev, barierova, cisloZony) VALUES (@nazev, @barierova, @cisloZony)";
        public String SQL_DELETE_ID = "DELETE FROM Zastavka WHERE idZastavky=@idZastavky";
        public String SQL_UPDATE = "UPDATE Zastavka SET idZastavky=@idZastavky, nazev=@nazev, barierova=@barierova, cisloZony=@cisloZony WHERE idZastavky=@idZastavky";

        public ZastavkaTable()
        {
        }

        public int VyhledatSpoj(String Odkud, String Kam, TimeSpan Cas)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand("EXECUTE vyhledej_spoj @odkud, @kam, @cas, @cas_odjezdu out, @cas_prijezdu out;");

            command.CommandType = CommandType.StoredProcedure;

            int ret = 0;
            try
            {
                command.Parameters.Add("@odkud", SqlDbType.VarChar, Odkud.Length);
                command.Parameters["@odkud"].Value = Odkud;

                command.Parameters.Add("@kam", SqlDbType.VarChar, Kam.Length);
                command.Parameters["@kam"].Value = Kam;

                command.Parameters.Add("@cas", SqlDbType.Time);
                command.Parameters["@cas"].Value = Cas;

                command.Parameters.Add("@cas_odjezdu", SqlDbType.Time);
                command.Parameters["@cas_odjezdu"].Direction = ParameterDirection.Output;

                command.Parameters.Add("@cas_prijezdu", SqlDbType.Time);
                command.Parameters["@cas_prijezdu"].Direction = ParameterDirection.Output;

                ret = db.ExecuteNonQuery(command);
                Console.WriteLine(command.Parameters["@cas_prijezdu"].Value.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                db.Close();
            }
            return ret;
            
        }

        public int Insert(Zastavka zastavka)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand("EXECUTE vlozZastavku @nazev, @barierova, @cisloZony, @cas, @cisloLinky;");
            int ret = 0;
            try
            {
                command.Parameters.Add(new SqlParameter("@nazev", SqlDbType.VarChar, zastavka.Nazev.Length));
                command.Parameters["@nazev"].Value = zastavka.Nazev;

                command.Parameters.Add(new SqlParameter("@barierova", SqlDbType.Bit));
                command.Parameters["@barierova"].Value = zastavka.Barierova;

                command.Parameters.Add(new SqlParameter("@cisloZony", SqlDbType.Int));
                command.Parameters["@cisloZony"].Value = zastavka.CisloZony;

                command.Parameters.Add(new SqlParameter("@cas", SqlDbType.VarChar, 6));
                command.Parameters["@cas"].Value = zastavka.Cas.Hours + ":" + zastavka.Cas.Minutes;

                command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
                command.Parameters["@cisloLinky"].Value = zastavka.CisloLinky;


                ret = db.ExecuteNonQuery(command);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                db.Close();
            }
            return ret;
        }

        public int Update(Zastavka zastavka)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_UPDATE);
            int ret = 0;
            try
            {
                PrepareCommand(command, zastavka);
                ret = db.ExecuteNonQuery(command);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("{0}", zastavka.CisloZony);
                Console.WriteLine("{0}", zastavka.Nazev);
            }
            finally
            {
                db.Close();
            }
            return ret;
        }

        private void PrepareCommand(SqlCommand command, Zastavka zastavka)
        {
            command.Parameters.Add(new SqlParameter("@idZastavky", SqlDbType.Int));
            command.Parameters["@idZastavky"].Value = zastavka.IdZastavky;

            command.Parameters.Add(new SqlParameter("@nazev", SqlDbType.VarChar, zastavka.Nazev.Length));
            command.Parameters["@nazev"].Value = zastavka.Nazev;

            command.Parameters.Add(new SqlParameter("@barierova", SqlDbType.Bit));
            command.Parameters["@barierova"].Value = zastavka.Barierova;

            command.Parameters.Add(new SqlParameter("@cisloZony", SqlDbType.Int));
            command.Parameters["@cisloZony"].Value = zastavka.CisloZony;
        }



        public Collection<Zastavka> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            SqlDataReader reader = db.Select(command);

            Collection<Zastavka> zastavky = Read(reader);
            reader.Close();
            db.Close();
            return zastavky;
        }


        public Zastavka Select(int idZastavky)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_SELECT_ID);

            command.Parameters.Add(new SqlParameter("@idZastavky", SqlDbType.Int));
            command.Parameters["@idZastavky"].Value = idZastavky;
            SqlDataReader reader = db.Select(command);

            Collection<Zastavka> zastavky = Read(reader);
            Zastavka zastavka = null;
            if (zastavky.Count == 1)
            {
                zastavka = zastavky[0];
            }
            reader.Close();
            db.Close();
            return zastavka;
        }



        public Collection<Zastavka> SelectJmen()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT_JMENA);
            SqlDataReader reader = db.Select(command);

            Collection<Zastavka> zastavky = ReadJmena(reader);
            reader.Close();
            db.Close();
            return zastavky;
        }

        public Collection<Zastavka> SelectJmenLinky(int cisloLinky)
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT_JMEN_LINKY);
            SqlDataReader reader = null;
            Collection<Zastavka> zastavky = null;
            try
            {
                command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
                command.Parameters["@cisloLinky"].Value = cisloLinky;
                reader = db.Select(command);
                zastavky = ReadJmena(reader);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                reader.Close();
            }
            return zastavky;
        }

        private Collection<Zastavka> ReadJmena(SqlDataReader reader)
        {
            Collection<Zastavka> zastavky = new Collection<Zastavka>();

            while (reader.Read())
            {
                Zastavka zastavka = new Zastavka();
                zastavka.Nazev = reader.GetString(0);

                zastavky.Add(zastavka);
            }
            return zastavky;
        }


        private Collection<Zastavka> Read(SqlDataReader reader)
        {
            Collection<Zastavka> zastavky = new Collection<Zastavka>();

            while (reader.Read())
            {
                Zastavka zastavka = new Zastavka();
                zastavka.IdZastavky = reader.GetInt32(0);
                zastavka.Nazev = reader.GetString(1);
                zastavka.Barierova = reader.GetBoolean(2);
                zastavka.CisloZony = reader.GetInt32(3);

                zastavky.Add(zastavka);
            }
            return zastavky;
        }

        public int Delete(int idZastavky)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand("EXECUTE delete_zastavky @idZastavky;");
            int ret = 0;
            try
            {
                command.Parameters.Add(new SqlParameter("@idZastavky", SqlDbType.Int));
                command.Parameters["@idZastavky"].Value = idZastavky;
                ret = db.ExecuteNonQuery(command);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                db.Close();
            }
            return ret;
        }
    }

}