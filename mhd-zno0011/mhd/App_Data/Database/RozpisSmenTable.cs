﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MHD.Database
{
    public class RozpisSmenTable
    {
        public static String TABLE_NAME = "RozpisSmen";
        public String SQL_SELECT = "SELECT * FROM RozpisSmen";

        public RozpisSmenTable()
        {
        }

        private void PrepareCommand(SqlCommand command, RozpisSmen rozpisSmen)
        {
            command.Parameters.Add(new SqlParameter("@datum", SqlDbType.DateTime));
            command.Parameters["@datum"].Value = rozpisSmen.Datum;

            command.Parameters.Add(new SqlParameter("@jmenoSmeny", SqlDbType.VarChar));
            command.Parameters["@jmenoSmeny"].Value = rozpisSmen.JmenoSmeny;

            command.Parameters.Add(new SqlParameter("@rc", SqlDbType.BigInt));
            command.Parameters["@rc"].Value = rozpisSmen.Rc;

            command.Parameters.Add(new SqlParameter("@typSmeny", SqlDbType.VarChar, rozpisSmen.TypSmeny.Length));
            command.Parameters["@typSmeny"].Value = rozpisSmen.TypSmeny;

        }


        public Collection<RozpisSmen> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            SqlDataReader reader = db.Select(command);

            Collection<RozpisSmen> rozpisSmeny = Read(reader);
            reader.Close();
            db.Close();
            return rozpisSmeny;
        }


        private Collection<RozpisSmen> Read(SqlDataReader reader)
        {
            Collection<RozpisSmen> rozpisSmeny = new Collection<RozpisSmen>();

            while (reader.Read())
            {
                RozpisSmen rozpisSmen = new RozpisSmen();
                rozpisSmen.Datum = reader.GetDateTime(0);
                rozpisSmen.JmenoSmeny = reader.GetString(1);
                rozpisSmen.Rc = reader.GetInt64(2);
                rozpisSmen.TypSmeny = reader.GetString(3);

                rozpisSmeny.Add(rozpisSmen);
            }
            return rozpisSmeny;
        }
    }

}