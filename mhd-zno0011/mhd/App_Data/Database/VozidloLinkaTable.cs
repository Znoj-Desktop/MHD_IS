﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MHD.Database
{
    public class VozidloLinkaTable
    {
        public static String TABLE_NAME = "VozidloLinka";
        public String SQL_SELECT = "SELECT * FROM VozidloLinka";
        public String SQL_SELECT_ID = "SELECT * FROM VozidloLinka WHERE cisloLinky=@cisloLinky and idVozidla=@idVozidla";
        public String SQL_INSERT = "INSERT INTO VozidloLinka VALUES (@od, @do, @idVozidla, @cisloLinky)";
        public String SQL_DELETE_VOZIDLA = "DELETE FROM VozidloLinka WHERE idVozidla=@idVozidla";
        public String SQL_DELETE_ID = "DELETE FROM VozidloLinka WHERE cisloLinky=@cisloLinky and idVozidla=@idVozidla";
        public String SQL_UPDATE = "UPDATE VozidloLinka SET od=@od, do=@do, idVozidla=@idVozidla, cisloLinky=@cisloLinky WHERE cisloLinky=@cisloLinky and idVozidla=@idVozidla";

        public VozidloLinkaTable()
        {
        }

        public int Insert(VozidloLinka vozidloLinka)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_INSERT);
            PrepareCommand(command, vozidloLinka);
            int ret = db.ExecuteNonQuery(command);
            db.Close();
            return ret;
        }

        public int Update(VozidloLinka vozidloLinka)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_UPDATE);
            PrepareCommand(command, vozidloLinka);
            int ret = db.ExecuteNonQuery(command);
            db.Close();
            return ret;
        }

        private void PrepareCommand(SqlCommand command, VozidloLinka vozidloLinka)
        {

            command.Parameters.Add(new SqlParameter("@od", SqlDbType.VarChar, 6));
            command.Parameters["@od"].Value = vozidloLinka.Od.Hours + ":" + vozidloLinka.Od.Minutes;

            command.Parameters.Add(new SqlParameter("@do", SqlDbType.VarChar, 6));
            command.Parameters["@do"].Value = vozidloLinka.Do.Hours + ":" + vozidloLinka.Do.Minutes;

            command.Parameters.Add(new SqlParameter("@idVozidla", SqlDbType.Int));
            command.Parameters["@idVozidla"].Value = vozidloLinka.IdVozidla;

            command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
            command.Parameters["@cisloLinky"].Value = vozidloLinka.CisloLinky;
            
        }


        public Collection<VozidloLinka> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            SqlDataReader reader = db.Select(command);

            Collection<VozidloLinka> vozidloLinky = Read(reader);
            reader.Close();
            db.Close();
            return vozidloLinky;
        }

        public Collection<VozidloLinka> SelectDetail(int idVozidla, int cisloLinky)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_SELECT_ID);

            command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
            command.Parameters["@cisloLinky"].Value = cisloLinky;

            command.Parameters.Add(new SqlParameter("@idVozidla", SqlDbType.Int));
            command.Parameters["@idVozidla"].Value = idVozidla;
            SqlDataReader reader = db.Select(command);

            Collection<VozidloLinka> vozidloLinky = Read(reader);
            
            reader.Close();
            db.Close();
            return vozidloLinky;
        }




        private Collection<VozidloLinka> Read(SqlDataReader reader)
        {
            Collection<VozidloLinka> vozidloLinky = new Collection<VozidloLinka>();

            while (reader.Read())
            {
                VozidloLinka vozidloLinka = new VozidloLinka();
                vozidloLinka.Od = reader.GetTimeSpan(0);
                vozidloLinka.Do = reader.GetTimeSpan(1);
                vozidloLinka.IdVozidla = reader.GetInt32(2);
                vozidloLinka.CisloLinky = reader.GetInt32(3);

                vozidloLinky.Add(vozidloLinka);
            }
            return vozidloLinky;
        }

        public int Delete(int cisloLinky, int idVozidla)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_DELETE_ID);

            command.Parameters.Add(new SqlParameter("@cisloLinky", SqlDbType.Int));
            command.Parameters["@cisloLinky"].Value = cisloLinky;

            command.Parameters.Add(new SqlParameter("@idVozidla", SqlDbType.Int));
            command.Parameters["@idVozidla"].Value = idVozidla;
            int ret = db.ExecuteNonQuery(command);

            db.Close();
            return ret;
        }

        public int Delete_vozidla(int idVozidla)
        {
            Database db = new Database();
            db.Connect();
            SqlCommand command = db.CreateCommand(SQL_DELETE_VOZIDLA);

            command.Parameters.Add(new SqlParameter("@idVozidla", SqlDbType.Int));
            command.Parameters["@idVozidla"].Value = idVozidla;
            int ret = db.ExecuteNonQuery(command);

            db.Close();
            return ret;
        }
    }

}