﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MHD.Database
{
    public class RozpisSmen
    {
        public DateTime Datum { get; set; }
        public string JmenoSmeny { get; set; }

        public long Rc { get; set; }
        public Ridic Ridic { get; set; }

        public string TypSmeny { get; set; }
        public Smena Smena { get; set; }


    }

}