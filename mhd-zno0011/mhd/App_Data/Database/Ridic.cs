﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MHD.Database
{
    public class Ridic
    {
        public long Rc { get; set; }
        public string Jmeno { get; set; }
        public string Prijmeni { get; set; }
        public string Bydliste { get; set; }
        public long Tel { get; set; }
        public string Pohlavi { get; set; }
        public string Heslo { get; set; }
        public int Plat { get; set; }

        public static int LEN_ATTR_jmeno = 30;
        public static int LEN_ATTR_prijmeni = 30;
        public static int LEN_ATTR_bydliste = 30;
        public static int LEN_ATTR_pohlavi = 4;
        public static int LEN_ATTR_heslo = 20;

    }

}