﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MHD.Database
{
    public class VozidloLinka
    {
        public TimeSpan Od { get; set; }
        public TimeSpan Do { get; set; }
        public int IdVozidla { get; set; }
        public Vozidlo Vozidlo { get; set; }
        public int CisloLinky { get; set; }
        public Linka Linka { get; set; }

    }

}