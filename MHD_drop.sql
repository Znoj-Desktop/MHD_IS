/*
Created: 15. 4. 2013
Modified: 15. 4. 2013
Model: er
Database: MS SQL Server 2012
*/


-- Drop relationships section -------------------------------------------------

ALTER TABLE [ridi] DROP CONSTRAINT [vozidlo_ridi]
go
ALTER TABLE [ridi] DROP CONSTRAINT [ridic_ridi]
go
ALTER TABLE [ZastavkaLinkaCas] DROP CONSTRAINT [zastavuje]
go
ALTER TABLE [ZastavkaLinkaCas] DROP CONSTRAINT [se_sklada]
go
ALTER TABLE [RozpisSmen] DROP CONSTRAINT [je_v]
go
ALTER TABLE [RozpisSmen] DROP CONSTRAINT [chodi]
go
ALTER TABLE [Vozidlo] DROP CONSTRAINT [stara_se_o]
go
ALTER TABLE [Zastavka] DROP CONSTRAINT [obsahuje]
go
ALTER TABLE [VozidloLinka] DROP CONSTRAINT [ma]
go
ALTER TABLE [VozidloLinka] DROP CONSTRAINT [jezdi]
go




-- Drop keys for tables section -------------------------------------------------

ALTER TABLE [ridi] DROP CONSTRAINT [Key1]
go
ALTER TABLE [ZastavkaLinkaCas] DROP CONSTRAINT [Unique_Identifier9]
go
ALTER TABLE [RozpisSmen] DROP CONSTRAINT [Unique_Identifier8]
go
ALTER TABLE [Smena] DROP CONSTRAINT [Unique_Identifier7]
go
ALTER TABLE [Ridic] DROP CONSTRAINT [Unique_Identifier6]
go
ALTER TABLE [Zona] DROP CONSTRAINT [Unique_Identifier5]
go
ALTER TABLE [Zastavka] DROP CONSTRAINT [Unique_Identifier4]
go
ALTER TABLE [Linka] DROP CONSTRAINT [Unique_Identifier3]
go
ALTER TABLE [VozidloLinka] DROP CONSTRAINT [Unique_Identifier2]
go
ALTER TABLE [Vozidlo] DROP CONSTRAINT [Unique_Identifier1]
go



-- Drop tables section ---------------------------------------------------

DROP TABLE [ridi]
go
DROP TABLE [ZastavkaLinkaCas]
go
DROP TABLE [RozpisSmen]
go
DROP TABLE [Smena]
go
DROP TABLE [Ridic]
go
DROP TABLE [Zona]
go
DROP TABLE [Zastavka]
go
DROP TABLE [Linka]
go
DROP TABLE [VozidloLinka]
go
DROP TABLE [Vozidlo]
go





