select * from Vozidlo;
select * from ridi;
select * from VozidloLinka;

declare @typLinky varchar(30), @cisloLinky integer;
--ridic, ktery se nestara o zadne vozidlo a lze jej priradit k novemu - pokud neni zadny volny, pak nelze vozidlo pridat
select * from ridic r where r.rc not in (Select rc from ridi);
--vozidlu je treba priradit linku -> vybirame z linek, kde typLinky odpovida typu pridavaneho vozidla (zde pro demonstraci vlak)
select @cisloLinky=cisloLinky from linka where linka.typLinky=@typLinky;
--pro vybranou linku urci jeji casovou delku
select top 1 DATEDIFF(mi, od, do) from VozidloLinka where cisloLinky=@cisloLinky;
--pro zadany cas odjezdu dopocita dle zadane linky cas prijezdu
select DATEADD(mi, 660, od) from VozidloLinka where cisloLinky=@cisloLinky;

insert into Vozidlo values(@idVozidla, @typVozidla, @znacka, @maxPocet, @barierove, @rc);

--nejdriv je treba odebrat veskere vazby vozidla, pak vozidlo
declare @idVozidla integer;
delete from VozidloLinka where idVozidla=@idVozidla;
delete from ridi where idVozidla=@idVozidla;
delete from Vozidlo where idVozidla=@idVozidla;


delete from ridi where rc=8205201235;
DODELAT--zde je potreba nahradit ridice jinym ridicem
select * from RozpisSmen where rc=8205201235;
delete from Ridic where rc=8205201235;



--umistit zastavku do libovolne Zony z vyberu
select * from Zona;
--umistit zastavku do Linky dle vyberu
select * from Linka;
declare @cisloLinky integer, @idZastavky integer, @cas datetime;
declare @nazev varchar(30), @barierova bit, @cisloZony integer;
--vybrat casy, kdy bude na dane zastavce linka stavet
insert into ZastavkaLinkaCas values(@cas, @cisloLinky, @idZastavky);
insert into Zastavka values(@idZastavky, @nazev, @barierova, @cisloZony);
select * from Zastavka;


select * from zastavka where idZastavky=@idZastavky;
select * from ZastavkaLinkaCas where idZastavky=@idZastavky;

declare @typLinky varchar(30);
insert into Linka values(@cisloLinky, @typLinky);

delete from VozidloLinka where cisloLinky=@cisloLinky;
delete from ZastavkaLinkaCas where cisloLinky=@cisloLinky;
delete from Linka where cisloLinky=@cisloLinky;



declare @nazev_odjezdu varchar(30), @nazev_prijezdu varchar(30), @zastavka_odjezdu integer, @zastavka_prijezdu integer;
declare @linka_odjezdu integer, @linka_prijezdu integer, @cas_odjezdu time;

set @nazev_odjezdu='Studentsk�';
set @nazev_prijezdu='Kr�tk�';
set @cas_odjezdu = CURRENT_TIMESTAMP;
select @linka_odjezdu=zlc.cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_odjezdu;
select @linka_prijezdu=zlc.cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_prijezdu;

--bez prestupu - kurzorem projit linky a vypsat zastavky na trase mezi zastavkou odjezdu a prijezdu
select * from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_odjezdu and cas<=@cas_odjezdu and zlc.cisloLinky in (select zlc.cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_prijezdu);

--kdyz se musi prestupovat, tak
--zastavky na kterych se da prestoupit - do kurzoru a rekurzivne zavolat tuto metodu s jednou hodnotou puvodni a druhou z kurzoru a naopak
select * from ZastavkaLinkaCas zlc where cas>=@cas_odjezdu and zlc.cisloLinky in
(select zlc.cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_odjezdu) 
and zlc.idZastavky in(Select idZastavky from ZastavkaLinkaCas where cisloLinky in 
(select zlc.cisloLinky from Zastavka z join ZastavkaLinkaCas zlc on zlc.idZastavky=z.idZastavky where z.nazev=@nazev_prijezdu))



select * from Zastavka;
select * from ZastavkaLinkaCas where idZastavky=4;
select * from ZastavkaLinkaCas where idZastavky=11;
select * from ZastavkaLinkaCas zlc where zlc.cisloLinky=@zastavka_odjezdu and zlc.idZastavky in(Select idZastavky from ZastavkaLinkaCas where cisloLinky=@zastavka_prijezdu)



declare @menenaLinka integer, @menenaTyp varchar(30);
declare @novyTyp varchar(30);
select @menenaTyp=typLinky from Linka where cisloLinky=@menenaLinka;
if @novyTyp!=@menenaTyp
begin
	begin tran
		delete ZastavkaLinkaCas where cisloLinky=@menenaLinka;
		delete VozidloLinka where cisloLinky=@menenaLinka;
		update Linka set typLinky=@novyTyp where cisloLinky=@menenaLinka;
		COMMIT;
end;

select * from VozidloLinka;



